For Gamers:
Neutron is a 2-player game played on 5 x 5 board. Game starting with five balls for each player in theirs bases(first and last row)
and Neutron in the middle of board. Point of game is to bring the Neutron(NN) player's base or to block Neutron with balls that enemy
can not move it. After starting game you need to write your name and choose game mode. Have fun!

For Programers:
Game is make from 3 files
1 file classes.py:

- class Ball:
	atributes:
		self._position1
        	self._position2
        	self._board 
        	self._name
        functions:
		get_name()
		get_position1()
		set_position1()
		get_position2()
		set_position2()
		get_board()
		choose_move()
		possible_moves()
		possibility_of_move_down()
		possibility_of_move_up() 
		possibility_of_move_right()
		possibility_of_move_left()
		possibility_of_move_up_right()
		possibility_of_move_up_left()
		possibility_of_move_down_left()
		possibility_of_move_down_right()

- class Neutron(Ball):
	functions:
		neutron_in_base()
		neutron_is_blocked()

-class Player:
	atributes:
		self._name  
        	self._ball_list 
        	self._balls_with_moves_dict 
        	self._base_position
	functions:
		get_name()
		get_ball_list()
		set_ball_list()
		get_balls_with_moves_dict()
		set_balls_with_moves_dict()
		get_base_position()
		take_ball_name()
		take_new_ball_position()
		find_balls_with_moves()
		choose_ball()
		set_balls()

-class Computer(Player):
	functions:
		take_ball_name()
		take_new_ball_position()

-class Thinking_Computer(Player):
	functions:
		take_ball_name()
		take_new_ball_position()
		find_possible_blocks()
		find_ball_with_possible_blocks()

2 file test_classes.py:
	include tests for classes.py

3 file game.py:
	functions:
		print_board()
		move_neutron()
		move_ball()
		round_of_game()
		choose_game_mode()
		set_enemy()
		main()





	