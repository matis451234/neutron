import random


class Ball:
    def __init__(self, position1, position2, board, name):
        self._position1 = position1
        self._position2 = position2
        self._board = board
        self._name = name
        self._board[self._position1][self._position2] = self._name

    def get_name(self):
        return self._name

    def get_position1(self):
        return self._position1

    def set_position1(self, new_position1):
        self._position1 = new_position1

    def get_position2(self):
        return self._position2

    def set_position2(self, new_position2):
        self._position2 = new_position2

    def get_board(self):
        return self._board

    def choose_move(self, new_position_1, new_position_2):
        bad_choice = True
        new_position = (new_position_1, new_position_2)
        if new_position in self.possible_moves():
            val = self.get_board()[self.get_position1()][self.get_position2()]
            self.get_board()[new_position_1][new_position_2] = val
            self.get_board()[self.get_position1()][self.get_position2()] = 0
            self.set_position1(new_position_1)
            self.set_position2(new_position_2)
            bad_choice = False
            return bad_choice
        else:
            print("No possibility of such move")
            return bad_choice

    def possible_moves(self):
        list_of_possible_moves = []
        actual_position = (self.get_position1(), self.get_position2())
        list_of_possible_moves.append(self.possibility_of_move_down_left())
        list_of_possible_moves.append(self.possibility_of_move_down())
        list_of_possible_moves.append(self.possibility_of_move_down_right())
        list_of_possible_moves.append(self.possibility_of_move_left())
        list_of_possible_moves.append(self.possibility_of_move_right())
        list_of_possible_moves.append(self.possibility_of_move_up_left())
        list_of_possible_moves.append(self.possibility_of_move_up())
        list_of_possible_moves.append(self.possibility_of_move_up_right())
        while list_of_possible_moves.count(actual_position) > 0:
            list_of_possible_moves.remove(actual_position)
        return list_of_possible_moves

    def possibility_of_move_down(self):
        y = self.get_position1()
        x = self.get_position2()
        y = y - 1
        while y >= 0:
            if self.get_board()[y][x] != 0:
                y = y + 1
                return y, x
            y = y - 1
        y = y + 1
        return y, x

    def possibility_of_move_up(self):
        y = self.get_position1()
        x = self.get_position2()
        y = y + 1
        while y <= 4:
            if self.get_board()[y][x] != 0:
                y = y - 1
                return y, x
            y = y + 1
        y = y - 1
        return y, x

    def possibility_of_move_right(self):
        y = self.get_position1()
        x = self.get_position2()
        x = x + 1
        while x <= 4:
            if self.get_board()[y][x] != 0:
                x = x - 1
                return y, x
            x = x + 1
        x = x - 1
        return y, x

    def possibility_of_move_left(self):
        y = self.get_position1()
        x = self.get_position2()
        x = x - 1
        while x >= 0:
            if self.get_board()[y][x] != 0:
                x = x + 1
                return y, x
            x = x - 1
        x = x + 1
        return y, x

    def possibility_of_move_up_right(self):
        y = self.get_position1()
        x = self.get_position2()
        y = y + 1
        x = x + 1
        while y <= 4 and x <= 4:
            if self.get_board()[y][x] != 0:
                y = y - 1
                x = x - 1
                return y, x
            y = y + 1
            x = x + 1
        y = y - 1
        x = x - 1
        return y, x

    def possibility_of_move_up_left(self):
        y = self.get_position1()
        x = self.get_position2()
        y = y + 1
        x = x - 1
        while y <= 4 and x >= 0:
            if self.get_board()[y][x] != 0:
                y = y - 1
                x = x + 1
                return y, x
            y = y + 1
            x = x - 1
        y = y - 1
        x = x + 1
        return y, x

    def possibility_of_move_down_left(self):
        y = self.get_position1()
        x = self.get_position2()
        y = y - 1
        x = x - 1
        while y >= 0 and x >= 0:
            if self.get_board()[y][x] != 0:
                y = y + 1
                x = x + 1
                return y, x
            y = y - 1
            x = x - 1
        y = y + 1
        x = x + 1
        return y, x

    def possibility_of_move_down_right(self):
        y = self.get_position1()
        x = self.get_position2()
        y = y - 1
        x = x + 1
        while y >= 0 and x <= 4:
            if self.get_board()[y][x] != 0:
                y = y + 1
                x = x - 1
                return y, x
            y = y - 1
            x = x + 1
        y = y + 1
        x = x - 1
        return y, x


class Neutron(Ball):
    def __init__(self, board):
        self._name = 'NN'
        self._position1 = 2
        self._position2 = 2
        self._board = board
        self._board[self._position1][self._position2] = self._name

    def neutron_in_base(self, player_1, player_2):
        if self.get_position1() == player_1.get_base_position():
            return player_1
        elif self.get_position1() == player_2.get_base_position():
            return player_2
        else:
            return None

    def neutron_is_blocked(self):
        if self.possible_moves() == []:
            return True


class Player():
    def __init__(self, name, base_position=0):
        self._name = name
        self._ball_list = []
        self._balls_with_moves_dict = {}
        self._base_position = base_position

    def get_name(self):
        return self._name

    def get_ball_list(self):
        return self._ball_list

    def set_ball_list(self, ball):
        self._ball_list.append(ball)

    def get_balls_with_moves_dict(self):
        return self._balls_with_moves_dict

    def set_balls_with_moves_dict(self, ball):
        self._balls_with_moves_dict[ball.get_name()] = ball

    def get_base_position(self):
        return self._base_position

    def take_ball_name(self, balls_dict, neutron):
        print(f'balls with moves: {list(balls_dict.keys())}')
        name = input('Write ball name:')
        return name

    def take_new_ball_position(self, choosen_ball, neutron):
        list_of_possible_moves = choosen_ball.possible_moves()
        print(f'(Y,X) Possible moves: {list_of_possible_moves}')
        y = input("Write Y argument")
        x = input("Write X argument")
        return (y, x)

    def find_balls_with_moves(self):
        for ball in self._ball_list:
            if ball.possible_moves() != []:
                self.set_balls_with_moves_dict(ball)
        return self.get_balls_with_moves_dict()

    def choose_ball(self, choosen_ball):
        bad_choice = True
        if choosen_ball in self.get_ball_list():
            bad_choice = False
            return bad_choice
        else:
            print("There is no such ball")
            return bad_choice

    def set_balls(self, board):
        if self.get_base_position() == 4:
            up_or_down = 4
            ball_name = "U"
        else:
            up_or_down = 0
            ball_name = "D"
        for i in range(0, 5):
            ball = Ball(up_or_down, i, board, ball_name + f"{i}")
            self.set_ball_list(ball)


class Computer(Player):
    def __init__(self, base_position=4):
        self._name = 'Random Computer'
        self._ball_list = []
        self._balls_with_moves_dict = {}
        self._base_position = base_position
 
    def take_ball_name(self, ball_dict, neutron):
        name = random.choice(list(ball_dict.keys()))
        return name

    def take_new_ball_position(self, choosen_ball, neutron):
        list_of_possible_moves = choosen_ball.possible_moves()
        random_move = random.choice(list_of_possible_moves)
        return random_move


class Thinking_Computer(Player):
    def __init__(self, base_position=4):
        self._name = 'Thinking Computer'
        self._ball_list = []
        self._balls_with_moves_dict = {}
        self._base_position = base_position

    def take_ball_name(self, ball_dict, neutron):
        ball = self.find_ball_with_possible_blocks(neutron, ball_dict)
        if ball:
            return ball.get_name()
        name = random.choice(list(ball_dict.keys()))
        return name

    def take_new_ball_position(self, choosen_ball, neutron):
        list_of_possible_moves = choosen_ball.possible_moves()
        enemy_position = self.get_base_position() - 4
        if type(choosen_ball) is Neutron:
            for move in list_of_possible_moves:
                if move[0] == self.get_base_position():
                    return move
                if move[0] == enemy_position:
                    if len(list_of_possible_moves) >= 2:
                        list_of_possible_moves.remove(move)
        else:
            possible_blocks = self.find_possible_blocks(neutron)
            for move in list_of_possible_moves:
                if move in possible_blocks:
                    return move
        random_move = random.choice(list_of_possible_moves)
        return random_move

    def find_possible_blocks(self, neutron):
        possible_blocks = []
        for move in neutron.possible_moves():
            enemy_position = self.get_base_position() - 4
            if move[0] == enemy_position:
                if move[1] == neutron.get_position2():
                    for i in range(enemy_position, neutron.get_position1()):
                        move = [i, neutron.get_position2()]
                        possible_blocks.append(move)
                    return possible_blocks
                if move[1] >= neutron.get_position2():
                    position2 = move[1]
                    for i in range(enemy_position, neutron.get_position1()):
                        move = [i, position2]
                        possible_blocks.append(move)
                        position2 -= 1
                    return possible_blocks
                if move[1] <= neutron.get_position2():
                    position2 = move[1]
                    for i in range(enemy_position, neutron.get_position1()):
                        move = [i, position2]
                        possible_blocks.append(move)
                        position2 += 1
                    return possible_blocks
        return possible_blocks

    def find_ball_with_possible_blocks(self, neutron, ball_dict):
        for ball in ball_dict.values():
            possible_blocks = self.find_possible_blocks(neutron)
            for position1, position2 in ball.possible_moves():
                move = [position1, position2]
                if move in possible_blocks:
                    return ball
