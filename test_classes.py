from classes import Ball, Neutron, Player, Thinking_Computer


def test_possibility_of_move_up():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_up() == (3, 2)


def test_possibility_of_move_down():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_down() == (1, 2)


def test_possibility_of_move_right():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_right() == (2, 4)


def test_possibility_of_move_left():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_left() == (2, 0)


def test_possibility_of_move_up_right():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_up_right() == (3, 3)


def test_possibility_of_move_up_left():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_up_left() == (3, 1)


def test_possibility_of_move_down_left():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_down_left() == (1, 1)


def test_possibility_of_move_down_right():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    assert Neutron.possibility_of_move_down_right() == (1, 3)


def test_possible_moves():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 3, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    Neutron = Ball(2, 2, Plansza, 'u')
    list_of_possible_moves = [(1, 1), (1, 2), (1, 3), (2, 0),
                              (2, 4), (3, 1), (3, 2), (3, 3)]
    assert Neutron.possible_moves() == list_of_possible_moves


def test_choose_move():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    neutron = Ball(2, 2, Plansza, 'u')
    result = neutron.choose_move(3, 1)
    assert result is False
    assert neutron._position1 == 3
    assert neutron._position2 == 1


def test_choose_move_move_not_possible():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    neutron = Ball(2, 2, Plansza, 'u')
    result = neutron.choose_move(0, 0)
    assert result is True
    assert neutron._position1 == 2
    assert neutron._position2 == 2


def test_neutron_is_blocked():
    list_4 = [1, 1, 1, 1, 1]
    list_3 = [0, 1, 1, 1, 0]
    list_2 = [0, 1, 0, 1, 0]
    list_1 = [0, 2, 2, 2, 0]
    list_0 = [2, 2, 2, 2, 2]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    neutron = Neutron(Plansza)
    result = neutron.neutron_is_blocked()
    assert result is True


def test_neutron_in_base0():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1')
    player2 = Player('player2', 4)
    neutron = Neutron(Plansza)
    neutron.choose_move(0, 2)
    result = neutron.neutron_in_base(player1, player2)
    assert result == player1


def test_neutron_in_base4():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1')
    player2 = Player('player2', 4)
    neutron = Neutron(Plansza)
    neutron.choose_move(4, 2)
    result = neutron.neutron_in_base(player1, player2)
    assert result == player2


def test_find_balls_with_moves():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1')
    player2 = Player('player2', 4)
    player1.set_balls(Plansza)
    player2.set_balls(Plansza)
    result = player1.find_balls_with_moves()
    assert list(result.values()) == player1.get_ball_list()


def test_find_balls_with_moves_no_moves():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [1, 1, 1, 1, 1]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1')
    player2 = Player('player2', 4)
    player1.set_balls(Plansza)
    player2.set_balls(Plansza)
    result = player1.find_balls_with_moves()
    assert list(result.values()) == []


def test_choose_ball():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1')
    player1.set_balls(Plansza)
    player1.find_balls_with_moves()
    choosen_ball = player1._balls_with_moves_dict.get('D0')
    result = player1.choose_ball(choosen_ball)
    assert result is False


def test_choose_ball_wrong_ball():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [1, 1, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1')
    player1.set_balls(Plansza)
    player1.find_balls_with_moves()
    choosen_ball = player1._balls_with_moves_dict.get('D0')
    result = player1.choose_ball(choosen_ball)
    assert result is True


def test_set_balls_down():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1')
    player1.set_balls(Plansza)
    assert Plansza[0] == ['D0', 'D1', 'D2', 'D3', 'D4']


def test_set_balls_set():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    player1 = Player('player1', 4)
    player1.set_balls(Plansza)
    assert Plansza[4] == ['U0', 'U1', 'U2', 'U3', 'U4']


def test_take_new_ball_position_Thinking_Computer_Neutron():
    list_4 = [0, 1, 1, 1, 1]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    TC = Thinking_Computer()
    neutron = Neutron(Plansza)
    result = TC.take_new_ball_position(neutron, neutron)
    assert result == (4, 0)


def test_take_new_ball_position_Thinking_Computer_Ball():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [1, 1, 1, 1, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    TC = Thinking_Computer()
    neutron = Neutron(Plansza)
    TC.set_balls(Plansza)
    result = TC.take_ball_name(TC.find_balls_with_moves(), neutron)
    assert result == 'U3'


def test_take_new_ball_position_Thinking_Computer_Ball1():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [1, 1, 1, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    TC = Thinking_Computer()
    neutron = Neutron(Plansza)
    TC.set_balls(Plansza)
    result = TC.take_ball_name(TC.find_balls_with_moves(), neutron)
    assert result == 'U4'


def test_take_new_ball_position_Thinking_Computer_Ball2():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 1, 1, 1, 1]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    TC = Thinking_Computer()
    neutron = Neutron(Plansza)
    TC.set_balls(Plansza)
    result = TC.take_ball_name(TC.find_balls_with_moves(), neutron)
    assert result == 'U0'


def test_take_new_ball_position_Thinking_Computer_Ball3():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [1, 1, 0, 1, 1]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    TC = Thinking_Computer()
    neutron = Neutron(Plansza)
    TC.set_balls(Plansza)
    TC.get_ball_list()[2].choose_move(2, 4)
    result = TC.take_ball_name(TC.find_balls_with_moves(), neutron)
    assert result == 'U2'
