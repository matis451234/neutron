from classes import Player, Computer, Thinking_Computer, Neutron


def print_board(board):
    counter = 4
    print('Y:')
    for row in reversed(board):
        new_row = []
        for position in row:
            if position == 0:
                position = '  '
            new_row.append(position)
        print(f'{counter} {new_row}')
        counter -= 1
    print('X:   0     1     2     3     4')


def move_neutron(first_second, neutron, board):
    print(f'{first_second.get_name()} turn, neutron move')
    bad_choice = True
    counter = 0
    while bad_choice:
        y, x = first_second.take_new_ball_position(neutron, neutron)
        try:
            y = int(y)
            x = int(x)
            bad_choice = neutron.choose_move(y, x)
        except:
            bad_choice = True
        counter += 1
        if counter == 5:
            counter -= 5
            print_board(board)
    print_board(board)


def move_ball(first_second, board,  neutron):
    bad_choice = True
    print(f'{first_second.get_name()} turn, ball move')
    balls_dict = first_second.find_balls_with_moves()
    counter = 0
    while bad_choice:
        choosen_ball_name = first_second.take_ball_name(balls_dict, neutron)
        choosen_ball = balls_dict.get(f'{choosen_ball_name}')
        bad_choice = first_second.choose_ball(choosen_ball)
        counter += 1
        if counter == 5:
            counter -= 5
            print_board(board)
    bad_choice = True
    counter = 0
    while bad_choice:
        y, x = first_second.take_new_ball_position(choosen_ball, neutron)
        try:
            y = int(y)
            x = int(x)
            bad_choice = choosen_ball.choose_move(y, x)
        except:
            bad_choice = True
        counter += 1
        if counter == 5:
            counter -= 5
            print_board(board)
    print_board(board)


def round_of_game(first_second, neutron, board, first, second):
    move_neutron(first_second, neutron, board)
    if neutron.neutron_in_base(first, second):
        return neutron.neutron_in_base(first, second)
    move_ball(first_second, board, neutron)
    if neutron.neutron_is_blocked():
        return first_second


def choose_game_mode():
    bad_choice = True
    message = ('Play with: '
               '1 - second player, '
               '2 - Random Computer, '
               '3 - Thinking Computer')
    counter = 4
    while bad_choice:
        counter += 1
        if counter == 5:
            counter -= 5
            print(message)
        game_mode = input("Write number to choose game mode")
        good_choice = ['1', '2', '3']
        if game_mode in good_choice:
            bad_choice = False
            return int(game_mode)
        else:
            print('There is no such game mode')


def set_enemy():
    game_mode = choose_game_mode()
    if game_mode == 1:
        name2 = input('Write second Player name')
        second = Player(name2, 4)
    elif game_mode == 2:
        second = Computer()
    elif game_mode == 3:
        second = Thinking_Computer()
    return second


def main():
    list_4 = [0, 0, 0, 0, 0]
    list_3 = [0, 0, 0, 0, 0]
    list_2 = [0, 0, 0, 0, 0]
    list_1 = [0, 0, 0, 0, 0]
    list_0 = [0, 0, 0, 0, 0]
    Plansza = [list_0, list_1, list_2, list_3, list_4]
    name = input("Write your name: ")
    first = Player(name)
    second = set_enemy()
    neutron = Neutron(Plansza)
    first.set_balls(Plansza)
    second.set_balls(Plansza)
    print_board(Plansza)
    winner = None
    move_ball(first, Plansza, neutron)
    while not winner:
        winner = round_of_game(second, neutron, Plansza, first, second)
        if not winner:
            winner = round_of_game(first, neutron, Plansza, first, second)
    print(f'{winner.get_name()} win')


if __name__ == "__main__":
    main()
